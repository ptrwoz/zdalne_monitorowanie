from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from .forms import CreateUserForm, UserChangePasswordForm, UpdateUserForm, UpdateProfileForm, UploadFile, ContactForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Profile, Assignment, Major, Group, TeacherSubject, TaskInfo, Subject, Raport
import mimetypes
import os
from django.conf import settings
from django.urls import reverse
from urllib.parse import urlencode

from django.contrib.auth.forms import PasswordChangeForm
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage, send_mail, BadHeaderError

#kalendarz
from datetime import datetime, date, timedelta
import calendar
from django.urls import reverse
from django.utils.safestring import mark_safe
from .utils import Calendar
from django.views import generic


def index(request):

    return render(request, "mainpage.html")

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Login lub hasło są niepoprawne')

        context = {}
        return render(request, "login.html", context)

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = CreateUserForm()

        if request.method == 'POST':
            form = CreateUserForm(request.POST)

            mail_exists = request.POST.get('email')
            if User.objects.filter(email=mail_exists).exists():
                messages.error(request, "Istnieje już konto o podanym emailu")
                return redirect('register')

            if form.is_valid():
                form.save()
                user = form.save(commit=False)
                user.is_active = False
                user.save()

                check_email = form.cleaned_data.get('email')
                if check_email.find('gmail') != -1:
                    Profile.objects.filter(userID=user.pk).update(roleID=2)

                current_site = get_current_site(request)
                mail_subject = 'Aktywuj swoje konto'
                message = render_to_string(
                    'acc_active_email.html', {
                        'user': user,
                        'domain': current_site.domain,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'token': default_token_generator.make_token(user),
                    }
                )
                to_mail = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_mail]
                )
                email.send()
                messages.success(request, 'Na Twojego maila wysłano link aktywacyjny')

                return redirect('login')

        context = {'form': form}
        return render(request, "register.html", context)


def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, 'Dziękuję za potwierdzenie mailowe. Teraz możesz się zalogować.')
        return redirect('login')
    else:
        messages.success(request, 'Link aktywacyjny jest niepoprawny')
        return redirect('login')


def logoutUser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def homePage(request):
    p = Profile.objects.get(userID=request.user)
    context = dict()
    if p.is_student():
        subjects_list = Assignment.objects.filter(userID=request.user).values_list('subjectID', flat=True)
        datetoday = datetime.today()
        context['tasks'] = TaskInfo.objects.filter(IdTeacherSubject__in=subjects_list, deadlineDate__range=[date.today(), datetoday + timedelta(days=25)]).order_by('deadlineDate')

        context['visible_first_name'] = 'hidden'
        context['visible_last_name'] = 'hidden'
        if not request.user.first_name:
            context['Data_first_name'] = 'Prosimy o uzupełnienie danych konta: imię'
            context['visible_first_name'] = 'visible'

        if not request.user.last_name:
            context['Data_last_name'] = 'Prosimy o uzupełnienie danych konta: nazwisko'
            context['visible_last_name'] = 'visible'

        if not context['tasks']:
            context['Infos'] = 'Nie masz żadnych sprawozdań do oddania'
        else:
            context['Infos'] = 'Najbliższe sprawozdania do oddania:'

        return render(request, "home.html", context)

    else:
        context['allTeacherSubs'] = TeacherSubject.objects.filter(TeacherOwner=request.user)
        context['visible_first_name'] = 'hidden'
        context['visible_last_name'] = 'hidden'
        if not request.user.first_name:
            context['Data_first_name'] = 'Prosimy o uzupełnienie danych konta: imię'
            context['visible_first_name'] = 'visible'

        if not request.user.last_name:
            context['Data_last_name'] = 'Prosimy o uzupełnienie danych konta: nazwisko'
            context['visible_last_name'] = 'visible'

        if not context['allTeacherSubs']:
            context['Info'] = 'Nie prowadzisz żadnych grup'
        else:
            context['Info'] = 'Grupy, które obecnie prowadzisz:'

        return render(request, "home.html", context)

@login_required(login_url='login')
def teacher_subject(request, IDTeacherSubject):

    all_students = Assignment.objects.filter(subjectID=IDTeacherSubject)
    current_teacher_sub = TeacherSubject.objects.get(id=IDTeacherSubject)
    all_raports = Raport.objects.filter(IdTaskInfo__IdTeacherSubject=IDTeacherSubject).order_by('studentID')

    name = []





    context = {'all_students': all_students,
               'current_teacher_sub': current_teacher_sub,
               'all_raports': all_raports,
               'name': name}

    if request.method == 'POST':
        raport_form = request.POST.get("btn")
        str_raport = str(raport_form)
        grade_form = request.POST.get(str_raport)

        obj = Raport.objects.get(id=raport_form)
        obj.grade = grade_form
        obj.save()
        return render(request, 'teacher_subject.html', context)

    return render(request, 'teacher_subject.html', context)

@login_required(login_url='login')
def settingsPage(request):

    hid_btn = request.POST.get("hid-btn")

    profiledata = Profile.objects.filter(userID=request.user)

    if request.method == 'POST' and hid_btn == '2':
        form_pass = UserChangePasswordForm(request.user, request.POST, prefix='pass-upd')
        if form_pass.is_valid():
            form_pass.save()
            update_session_auth_hash(request, form_pass.user)
            messages.success(request, 'Twoje hasło zostało zmienione')
            return redirect('settings')
    else:
        form_pass = UserChangePasswordForm(request.user, prefix='pass-upd')

    if request.method == 'POST' and hid_btn == '1':
        form_pass = UserChangePasswordForm(request.user, prefix='pass-upd')

        first_name = request.POST.get("first_name")
        last_name = request.POST.get("last_name")
        email = request.POST.get("email")
        index = request.POST.get("index")
        startcollegeyear = request.POST.get("startcollegeyear")
        currentsemester = request.POST.get("currentsemester")

        u = request.user
        u.first_name = first_name
        u.last_name = last_name
        u.email = email
        u.save()

        p = Profile.objects.get(userID=request.user)
        p.index = index
        p.startcollegeyear = startcollegeyear
        p.currentsemester = currentsemester
        p.save()

        messages.success(request, 'Twoje dane zostały zaktualizowane')
        return redirect('settings')

    context = {'profiledata': profiledata, 'form_pass': form_pass}

    return render(request, "settings.html", context)

@login_required(login_url='login')
def subjectPage(request):
        p=Profile.objects.get(userID=request.user)
        if p.is_student():
            hid_btn = request.POST.get("hid-btn")

            major_list = Major.objects.all()
            possible_subjects = None

            possible_subjects = TeacherSubject.objects.all()

            if request.method == 'POST' and hid_btn == "1":
                major_form = request.POST.get("major-form")
                semester_form = request.POST.get("semester-form")
                year_form = request.POST.get("year-form")

                possible_subjects = TeacherSubject.objects.filter(semester=semester_form, year=year_form, SubjectID__majorID__name__contains=major_form)

            if request.method == 'POST' and hid_btn == "2":
                chosen_id = request.POST.get("chosen")
                chosen_subject = TeacherSubject.objects.get(pk=chosen_id)

                if Assignment.objects.filter(userID=request.user, subjectID=chosen_subject).exists():
                    messages.error(request, "Jesteś już zapisany na ten kurs!")
                    return redirect('subjects')

                Assignment.objects.create(subjectID=chosen_subject, userID=request.user)
                return redirect('tasks')

            context = {'major_list': major_list, 'possible_subjects': possible_subjects}
            return render(request, "subjects.html", context)

        # tu kod dla teacher
        else:
            major_list = Major.objects.all()
            hid_btn = request.POST.get("hid-btn-teacher")
            context = {'major_list': major_list}

            if request.method == 'POST' and hid_btn == "3":
                major_form = request.POST.get("major-form")
                teacher_subjects_list = Subject.objects.filter(majorID__name=major_form)
                group_list = Group.objects.all()
                context = {'major_list': major_list, 'teacher_subjects_list': teacher_subjects_list, 'group_list': group_list, 'major_form': major_form}
                return render(request, "subjects_teacher.html", context)

            if request.method == 'POST' and hid_btn == "4":
                major_form = request.POST.get("secret_major")
                semester_form = request.POST.get("semester-form")
                year_form = request.POST.get("year-form")
                group_form = request.POST.get("group-form")
                subject_form = request.POST.get("subject-teacher-form")

                subject_id = Subject.objects.get(name=subject_form, majorID__name=major_form)
                group_id = Group.objects.get(id=group_form)

                newTeacherSubject = TeacherSubject()
                newTeacherSubject.TeacherOwner = request.user
                newTeacherSubject.SubjectID = subject_id
                newTeacherSubject.year = year_form
                newTeacherSubject.groupID = group_id
                newTeacherSubject.semester = semester_form
                newTeacherSubject.save()

                messages.success(request, 'Utworzyłeś nowy przedmiot')
                return render(request, "subjects_teacher.html", context)

            return render(request, "subjects_teacher.html", context)


@login_required(login_url='login')
def tasksPage(request):
    p = Profile.objects.get(userID=request.user)
    if p.is_student():

        user_assignments = Assignment.objects.filter(userID=request.user)
        hid_btn = request.POST.get("hid-btn")

        if request.method == 'POST' and hid_btn == '1':
            chosen_id = request.POST.get("chosen")
            taskinfo_list = TaskInfo.objects.filter(IdTeacherSubject=chosen_id)
            chosen_assignment = Assignment.objects.filter(userID=request.user, subjectID__id=chosen_id)
            print(chosen_assignment)
            context = {'taskinfo_list': taskinfo_list, 'chosen_assignment': chosen_assignment}
            return render(request, "tasks.html", context)

        if request.method == 'POST' and hid_btn == '2':
            chosen_task = request.POST.get("chosen_task")
            return redirect(task_info, chosen_task)

        context = {'user_assignments': user_assignments}
        return render(request, "tasks.html", context)
    else:
        currentteachersubjects = TeacherSubject.objects.filter(TeacherOwner=request.user)
        hid_btn = request.POST.get("hid-btn")
        style_value = 'hidden'

        if request.method == 'POST' and hid_btn == '1':
            chosen_id = request.POST.get("chosen")
            taskinfo_list = TaskInfo.objects.filter(IdTeacherSubject=chosen_id)
            style_value = 'visible'
            context = {'taskinfo_list': taskinfo_list, 'style_value': style_value, 'chosen_id': chosen_id}
            return render(request, "tasks_teacher.html", context)

        if request.method == 'POST' and hid_btn == '2':
            teacher_sub_id = request.POST.get("add_new")
            chosen_teacher_sub = TeacherSubject.objects.get(id=teacher_sub_id)

            context = {'chosen_teacher_sub': chosen_teacher_sub}
            return redirect('create_task', teacher_sub_id)

        context = {'currentteachersubjects': currentteachersubjects, 'style_value': style_value}
        return render(request, "tasks_teacher.html", context)


class CalendarView(generic.ListView):

    template_name = 'calendar.html'
    model = Assignment

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context = super().get_context_data(**kwargs)
        d = get_date(self.request.GET.get('month', None))
        cal = Calendar(d.year, d.month)
        html_cal = cal.formatmonth(self.request,withyear=True)
        context['calendar'] = mark_safe(html_cal)
        context['prev_month'] = prev_month(d)
        context['next_month'] = next_month(d)

        return context

def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return date(year, month, day=1)
    return datetime.today()
def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month

def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month

@login_required(login_url='login')
def upload_file(request):
    if request.method == "POST":
        form = UploadFile(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['Files']
            if(file.size/1000000 > 50):
                messages.success(request,  "Plik zbyt duży")
            else:
                handle_uploaded(file, "name")
                messages.success(request,  "Plik został wysłany na serwer")
                return HttpResponseRedirect("/tasks")
    else:
        form =  UploadFile()
    return render(request, 'fileupload.html', {'form': form})


def handle_uploaded_teacher(f, name):
    i=0
    nam = str(settings.BASE_DIR)+'\\Files\\Tasks\\'+name
    os.path.exists(nam)
    while(1):
        if(os.path.exists(nam+'_'+str(i)+'_'+f.name)):
            i+=1
        else:
            nam = nam+'_'+str(i)+'_'+f.name
            break;

    with open(nam, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return name+'_'+str(i)+'_'+f.name


def handle_uploaded(f, name):
    i=0
    nam = str(settings.BASE_DIR)+'\\Files\\Raports\\'+name
    os.path.exists(nam)
    while(1):
        if(os.path.exists(nam+'_'+str(i)+'_'+f.name)):
            i+=1
        else:
            nam = nam+'_'+str(i)+'_'+f.name
            break;

    with open(nam, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return name+'_'+str(i)+'_'+f.name

@login_required(login_url='login')
def task_info(request, IDTaskInfo):
    """
    Tutaj trzeba srpawdzić czy jakiś śmieszny człowiek nie stwierdzi
    czy może sobie wbije na inny task z id
    Traz jak wbije to go przeniesie na inną podstrone
    """
    temp = []
    context = dict()
    profile = Profile.objects.get(userID=request.user)
    asigments = Assignment.objects.filter(userID = profile.userID)
    try:
        task = TaskInfo.objects.get(id=IDTaskInfo)
    except:
        return HttpResponseRedirect('/tasks')
    for i in asigments:
         if(i.subjectID == task.IdTeacherSubject):
            if profile.is_student():
                rap = Raport.objects.filter(studentID=request.user, IdTaskInfo=task.id)
                if request.method == "POST":
                    form = UploadFile(request.POST, request.FILES)
                    if form.is_valid():
                        file = request.FILES['Files']
                        if (file.size / 1000000 > 50):
                            messages.success(request, "Plik zbyt duży")
                        else:
                            name = profile.index
                            s= handle_uploaded(file, name)
                            raap = Raport(studentID = request.user,insertDate = date.today(),IdTaskInfo = task, raport = s, grade=-1)
                            raap.save()
                            messages.success(request, "Plik został wysłany na serwer")
                            return HttpResponseRedirect("/taskinfo="+str(IDTaskInfo))

                name = str(task.Task).split('/')
                context['task_download_url'] = name[-1]
                context['Comment'] = str(task.Comment)
                context['DeadlineDate'] = task.deadlineDate
                subject =  str(task.IdTeacherSubject.SubjectID) + ' ' + str(task.IdTeacherSubject.TeacherOwner)
                context['Task_Name'] = subject
                context['uploaded_raports'] = ""
                temp = []
                for z in range(len(rap)):
                    if rap[z].grade == -1:
                        grad = "Nieoceniono"
                    else:
                        grad = str(rap[z].grade)
                    temp.append("Ocena z sprawozdania nr"+str(z+1)+": "+grad)
                context['uploaded_raports'] = temp

                if len(rap) == 0 or (len(rap)==1 and not rap[0].grade ==-1):
                    context['Form'] = UploadFile()['Files']
                    context['Button_visible'] = "visible"
                else:
                    context['Form']=""
                    context['Button_visible'] = "hidden"
                return render(request, 'taskInfo.html',  context)
    return HttpResponseRedirect('/tasks')

@login_required(login_url='login')
def download_file(request, filename):
    p = Profile.objects.get(userID=request.user)
    file_path = str(settings.BASE_DIR)+ "\\Files\\Tasks\\"+ str(filename)
    context = dict()
    context['task_download_url'] = file_path
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/liquid")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404

@login_required(login_url='login')
def download_file_raport(request, filename):
    p = Profile.objects.get(userID=request.user)
    file_path = str(settings.BASE_DIR)+ "\\Files\\Raports\\"+ str(filename)
    context = dict()
    context['task_download_url'] = file_path
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/liquid")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404

@login_required(login_url='login')
def createTask(request, IDTaskInfo):

    if request.method == "POST":
        form = UploadFile(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['Files']
            if (file.size/1000000 > 50):
                messages.success(request, "Plik zbyt duży")
            else:
                u = request.user

                newTaskInfo = TaskInfo()
                newTaskInfo.name = request.POST.get("taskname")
                teacherSub = TeacherSubject.objects.get(id=IDTaskInfo)
                newTaskInfo.IdTeacherSubject = teacherSub
                newTaskInfo.Comment = request.POST.get("comment")
                newTaskInfo.deadlineDate = request.POST.get("deadline_date")
                filename = u.first_name + u.last_name
                s = handle_uploaded_teacher(file, filename)
                newTaskInfo.Task = s
                newTaskInfo.save()

                messages.success(request, "Zadanie zostało utworzone")
                return redirect('tasks')
    else:
        form = UploadFile()

    context = {'form': form}
    return render(request, "create_task.html", context)

def editTask(request, IDTaskInfo):
    taskinfo = TaskInfo.objects.get(id=IDTaskInfo)
    print(taskinfo)
    print(taskinfo.deadlineDate)

    if request.method == "POST":
        form = UploadFile(request.POST, request.FILES)
        if form.is_valid():
            file = request.FILES['Files']
            if (file.size / 1000000 > 50):
                messages.success(request, "Plik zbyt duży")
            else:
                u = request.user

                newTaskInfo = TaskInfo()
                newTaskInfo.name = request.POST.get("taskname")
                teacherSub = TeacherSubject.objects.get(id=IDTaskInfo)
                newTaskInfo.IdTeacherSubject = teacherSub
                newTaskInfo.Comment = request.POST.get("comment")
                newTaskInfo.deadlineDate = request.POST.get("deadline_date")
                filename = u.first_name + u.last_name
                s = handle_uploaded_teacher(file, filename)
                newTaskInfo.Task = s
                newTaskInfo.save()

                messages.success(request, "Zadanie zostało utworzone")
                return redirect('tasks')
    else:
        form = UploadFile()

    task = TaskInfo.objects.get(id=IDTaskInfo)
    name = str(task.Task).split('/')

    context = {'form': form, 'taskinfo': taskinfo, 'task_download_url': name[-1]}
    return render(request, "edit_task.html", context)

def contactPage(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message'] + '\n\nWiadomość od: ' + email

            try:
                send_mail(subject, message, settings.EMAIL_HOST_USER, [settings.EMAIL_HOST_USER])
            except BadHeaderError:
                return HttpResponseRedirect('Błąd')
            messages.success(request, 'Wiadomość wysłana pomyślnie.')
            return redirect('contact')

    return render(request, "contact.html", {'form' : form})
