from django.contrib import admin

from .models import *

# Register your models here.

admin.site.register(Assignment)
admin.site.register(Role)
admin.site.register(Major)
admin.site.register(Profile)
admin.site.register(Raport)
admin.site.register(GroupType)
admin.site.register(Group)
admin.site.register(Message)
admin.site.register(Receiver)
admin.site.register(SupportContact)
admin.site.register(Subject)
admin.site.register(TeacherSubject)
admin.site.register(TaskInfo)
