from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib.auth.models import User
from .models import Profile, Raport
from django import forms
from django.core.validators import EmailValidator

class CreateUserForm(UserCreationForm):
    username = forms.CharField(label='Login', max_length=40, min_length=5, required=True, error_messages={
        'unique': 'Konto o podanym loginie już istnieje',
    }, widget=forms.TextInput(attrs={'class': 'inp1', 'placeholder': 'Login'}))
    email = forms.EmailField(label='Email', max_length=254, required=True, widget=forms.TextInput(attrs={'class': 'inp1', 'placeholder': 'E-mail'}))
    password1 = forms.CharField(label='Hasło', widget=forms.PasswordInput(attrs={'class': 'inp1', 'placeholder': 'Hasło'}), required=True)
    password2 = forms.CharField(label='Powtórz hasło', widget=forms.PasswordInput(attrs={'class': 'inp1', 'placeholder': 'Powtórz hasło'}), required=True)

    error_messages = {
        'password_mismatch': "Wprowadzone hasła nie są identyczne"
    }

    def clean_email(self):
        data = self.cleaned_data['email']
        domain = data.split('@')[1]
        domain_list = ["gmail.com", "stud.prz.edu.pl", "prz.edu.pl", ]
        if domain not in domain_list:
            raise forms.ValidationError("Proszę wprowadzić adres email z dozwoloną domeną")
        return data

    def save(self, commit=True):
        user = super(CreateUserForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']



class UserChangePasswordForm(PasswordChangeForm):
    old_password = forms.CharField(label='Stare hasło', required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder': 'Stare hasło'}),
                                   error_messages={'required': 'Pole stare hasło jest wymagane'})
    new_password1 = forms.CharField(label='Nowe hasło', required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder': 'Nowe hasło'}),
                                   error_messages={'required': 'Pole nowe hasło jest wymagane'})
    new_password2 = forms.CharField(label='Powtórz nowe hasło', required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder': 'Powtórz nowe hasło'}),
                                   error_messages={'required': 'Pole powtórz nowe hasło jest wymagane'})

    error_messages = {
        'password_mismatch': 'Wprowadzone nowe hasła nie są identyczne',
        'password_incorrect': 'Wprowadzone hasło nie jest poprawne'
    }
    # DODANIE ERRORÓW PO POLSKU

    class Meta:
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']


class UpdateUserForm(ModelForm):
    first_name = forms.CharField(label='First_name', max_length=40, required=False)
    last_name = forms.CharField(label='Last_name', max_length=40, required=False)
    email = forms.EmailField(label='Email', max_length=254, required=False)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

class UpdateProfileForm(ModelForm):
    index = forms.CharField(label='Index', max_length=6, required=False)
    startcollegeyear = forms.IntegerField(label='Rok rozpoczecia nauki', required=False)
    currentsemester = forms.IntegerField(label='Obecny semestr', required=False)

    class Meta:
        model = Profile
        fields = ['index', 'startcollegeyear', 'currentsemester']

class UploadFile(forms.Form):
    Files = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

class ContactForm(forms.Form):

    email = forms.EmailField(label='Adres e-mail', min_length=1, required=True, widget=forms.TextInput(attrs={'class': 'inp1', 'placeholder': 'Adres e-mail'}))
    subject = forms.CharField(label='Temat', min_length=1, required=True,
    widget=forms.TextInput(attrs={'class': 'inp1', 'placeholder': 'Temat'}))
    message = forms.CharField(label='Wiadomość', min_length=1, required=True,
    widget=forms.Textarea(attrs={'class': 'inp1', 'placeholder': 'Wiadomość'}))

