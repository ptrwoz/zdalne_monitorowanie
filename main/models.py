from django.db import models
from django.conf import settings
from django.shortcuts import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

class GroupType(models.Model):
    name = models.CharField(max_length=12)
    shortname = models.CharField(max_length=3)

    def __str__(self):
        return self.shortname

class Role(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class Major(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class Subject(models.Model):
    majorID = models.ForeignKey(Major, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name + ' ' + str(self.majorID)

class Group(models.Model):
    type = models.ForeignKey(GroupType, on_delete=models.CASCADE)
    number = models.IntegerField()

    def __str__(self):
        return str(self.type) + str(self.number)

class TeacherSubject(models.Model):
    TeacherOwner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='Teacher_owner')
    SubjectID = models.ForeignKey(Subject, on_delete=models.CASCADE)
    semester = models.IntegerField(blank=True, null=True)
    groupID = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, blank=True, related_name='Group_ID')
    year = models.IntegerField(default=2020)

    def __str__(self):
        return self.TeacherOwner.first_name + ' ' + self.TeacherOwner.last_name + ' ' + str(self.SubjectID) + ' ' + str(self.groupID)

class Assignment(models.Model):
    subjectID = models.ForeignKey(TeacherSubject, on_delete=models.CASCADE, blank=True, null=True)
    userID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True, related_name='Student_ID_ass')

    def __str__(self):
        return str(self.subjectID) + ' ' + str(self.userID)


DEFAULT_ROLE = 1


class Profile(models.Model):
    userID = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    index = models.CharField(max_length=6, blank=True, null=True, default='000000')
    startcollegeyear = models.IntegerField(default=2020, blank=True, null=True)
    currentsemester = models.IntegerField(default=1, blank=True, null=True)
    roleID = models.ForeignKey(Role, on_delete=models.CASCADE, blank=True, null=True, default=DEFAULT_ROLE)

    def __str__(self):
        return str(self.userID)

    @receiver(post_save, sender=settings.AUTH_USER_MODEL)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(userID=instance)
        instance.profile.save()

    def is_student(self):
        #print (self.roleID)
        if str(self.roleID) == "Student":
            return True
        else:
            return False

    def is_teacher(self):
        if str(self.roleID) == "Teacher":
            return True
        else:
            return False


class TaskInfo(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    IdTeacherSubject = models.ForeignKey(TeacherSubject, related_name='TeacherSubject', on_delete=models.CASCADE, blank=True, null=True)
    Task = models.FileField(upload_to='Files/Tasks/', blank=True, null=True)
    Comment = models.CharField(max_length=300)
    deadlineDate = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return str(self.name) + ' ' + str(self.IdTeacherSubject)

class Raport(models.Model):
    studentID = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='Student_ID', on_delete=models.CASCADE, blank=True, null=True)
    raport = models.FileField(upload_to='Files/Raports/', blank=True, null=True)
    grade = models.IntegerField(blank=True, null=True)
    correction = models.ForeignKey('self', on_delete=models.CASCADE, related_name='CorrectionRaport', blank=True, null=True)
    insertDate = models.DateTimeField(blank=True, null=True)
    IdTaskInfo = models.ForeignKey(TaskInfo, on_delete=models.CASCADE, blank=True, null=True, related_name='TaskInfo')

    def __str__(self):
        return str(self.studentID) + str(self.IdTaskInfo)


# Jeszcze nie używane
class Message(models.Model):
    content = models.CharField(max_length=400)
    userFromID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.userFromID

class Receiver(models.Model):
    messageID = models.ForeignKey(Message, on_delete=models.CASCADE)
    groupID = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)
    userID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.messageID


class SupportContact(models.Model):
    comment = models.CharField(max_length=400)
    userID = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.userID
