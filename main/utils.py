from datetime import datetime, date
from calendar import HTMLCalendar
from .models import *
from django.utils.safestring import mark_safe


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    def formatday(self, day, request, events, events_title, events_ID):

        d = ''
        r=len(events)
        for i in range (0,r,1):
            if day == events[i]:
                d += f'<li class="calendar_list"> <a style="text-decoration: none; color:red" href="/taskinfo={events_ID[i]}">{events_title[i]} </a></li>'
                i += 1
        if day != 0:
            return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
        return '<td></td>'

    def formatweek(self, theweek, request, events, events_title, events_ID):
        week = ''
        for d, weekday in theweek:
            week += self.formatday(d, request, events, events_title, events_ID)
        return f'<tr> {week} </tr>'

    def formatmonth(self, request,  withyear=True ):
        subjects_list = Assignment.objects.filter(userID=request.user).values_list('subjectID', flat=True)
        info_task = TaskInfo.objects.filter(IdTeacherSubject__in=subjects_list).values_list('deadlineDate', flat=True)
        title_task = TaskInfo.objects.filter(IdTeacherSubject__in=subjects_list).values_list('name', flat=True)
        info_task_ID=TaskInfo.objects.filter(IdTeacherSubject__in=subjects_list).values_list('id', flat=True)
        events = []
        events_title=[]
        events_ID=[]
        a = len(info_task)
        for i in range(0, a, 1):
            if info_task[i].month == self.month:
                events.append(info_task[i].day)
                events_title.append(title_task[i])
                events_ID.append((info_task_ID[i]))
        #print(events)
        #print(events_title)
        cal = f'<table border="0" cellpadding="0" cellspacing="0"     class="calendar">\n'
        cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week, request, events, events_title, events_ID)}\n'
        return cal
