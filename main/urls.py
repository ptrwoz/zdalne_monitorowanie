from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.loginPage, name='login'),
    path('logout', views.logoutUser, name='logout'),
    path('register', views.registerPage, name='register'),
    path('home', views.homePage, name='home'),
    path('settings', views.settingsPage, name='settings'),
    path('subjects', views.subjectPage, name='subjects'),
    path('tasks', views.tasksPage, name='tasks'),
    path('activate/<uidb64>/<token>/', views.activate, name='activate'),
    path('calendar', views.CalendarView.as_view(), name='calendar'),
    path('fileupload', views.upload_file, name='fileupload'),
    path('taskinfo=<int:IDTaskInfo>', views.task_info, name='task_info'),
    path('Files/Tasks=<str:filename>', views.download_file, name='download'),
    path('create_task=<int:IDTaskInfo>', views.createTask, name='create_task'),
    path('edit_task=<int:IDTaskInfo>', views.editTask, name='edit_task'),
    path('teacher_subject=<int:IDTeacherSubject>', views.teacher_subject, name='teacher_subject'),
    path('Files/Raports=<str:filename>', views.download_file_raport, name='download_raport'),
    path('contact', views.contactPage, name='contact')
]